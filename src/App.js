import React from 'react';
import './App.css';
import {
  BrowserRouter as Router, Switch, Route, Link
} from "react-router-dom";
import { Books } from './components/Books';
import { Characters } from './components/Characters';
import { Houses } from './components/Houses';
import { Button, List, Container } from 'semantic-ui-react';


function App() {

  const Home = () => {
    return (
      <h3>What are you interested in?</h3>
    );
  }

  return (
    <div className="App">
      <Router>
        <div>
          <h3>Welcome to the world of</h3>
          <h1>GAME OF THRONES</h1>

          <Container >
            <List horizontal animated verticaAlign='middle' style={{ marginTop: "50px" }}  >
              <List.Item>
                <Link to={"/"}><Button inverted color="blue">Home </Button></Link>
              </List.Item>
              <List.Item>
                <Link to={"/books"}>  <Button inverted color="green" >Books</Button></Link>
              </List.Item>
              <List.Item>
                <Link to={"/characters"}> <Button inverted color="red" >Characters</Button></Link>
              </List.Item>
              <List.Item>
                <Link to={"/houses"}><Button inverted color="yellow">Houses</Button></Link>
              </List.Item>
            </List>
          </Container>

          <Switch>

            <Route exact path="/" component={Home} default />
            <Route exact path="/books" component={Books} />

            <Route exact path="/characters" component={Characters} />

            <Route exact path="/houses" component={Houses} />


          </Switch>

        </div>
      </Router>
    </div>
  );
}

export default App;
