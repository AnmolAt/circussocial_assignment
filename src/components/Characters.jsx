import React, { useState } from 'react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import { Button } from 'semantic-ui-react';
import './Components.css';


export const Characters = () => {

  const [characters, setCharacters] = useState(null);

  const apiURL = "https://anapioficeandfire.com/api/characters/?page=2&pageSize=30";

  const fetchData = async () => {
    const response = await axios.get(apiURL)
    setCharacters(response.data)
  }



  return (
    <div className="main">

      <div>
        <Button className="btn" onClick={fetchData} color="red" style={{ marginTop: "20px" }}>
          Click to display list of GOT Characters
        </Button>
      </div>
      <div className="box">
        {characters &&
          characters.map((character, index) => {


            return (
              <div className="component" key={index}>
                <h1> {index + 1}</h1>
                <h2 style={{ color: "Blue" }}>{character.name}</h2>

                <div className="details">
                  <p> Gender: <strong><em>{character.gender}</em></strong> </p>
                  <p> Culture: <strong><em>{character.culture}</em> </strong> </p>
                  <p> DOB:  <strong><em> {character.born}</em> </strong></p>
                  <p> Played By: <strong><em>  {character.playedBy}</em> </strong></p>
                </div>
              </div>
            );
          })}
      </div>

    </div>
  );
}

