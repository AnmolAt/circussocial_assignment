import React, { useState } from 'react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import { Button } from 'semantic-ui-react';
import './Components.css';


export const Houses = () => {

  const [houses, setHouses] = useState(null);

  const apiURL = "https://anapioficeandfire.com/api/houses?page=1&pageSize=30";

  const fetchData = async () => {
    const response = await axios.get(apiURL)
    setHouses(response.data)
  }



  return (
    <div className="main">

      <div>
        <Button className="btn" color="yellow" onClick={fetchData} style={{ marginTop: "20px" }}>
          Click to display list of GOT Houses
        </Button>
      </div>
      <div className="box">
        {houses &&
          houses.map((house, index) => {


            return (
              <div className="component" key={index}>
                <h3>House {index + 1}</h3>
                <h2 style={{ color: "Blue" }}>{house.name}</h2>

                <div className="details">
                  <p> Region: <strong><em>{house.region}</em></strong> </p>
                  <p> Coat of Arms: <strong><em>{house.coatOfArms}</em> </strong></p>
                  <p> Words:  <strong><em> {house.words}</em> </strong></p>
                  <p> Current Lord: <strong><em>  {house.currentLord}</em> </strong></p>
                </div>
              </div>
            );
          })}
      </div>

    </div>
  );
}

