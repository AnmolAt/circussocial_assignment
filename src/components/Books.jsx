import React, { useState } from 'react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import { Button } from 'semantic-ui-react';
import './Components.css';


export const Books = () => {

  const [books, setBooks] = useState(null);


  const apiURL = "https://www.anapioficeandfire.com/api/books?page=1 &pageSize=30";

  const fetchData = async () => {
    const response = await axios.get(apiURL);
    setBooks(response.data)
  }



  return (
    <div className="main">

      <div>
        <Button color="green" onClick={fetchData} style={{ marginTop: "20px" }} >
          Click to display list of GOT Books
        </Button>
      </div>
      <div className="box">
        {books &&
          books.map((book, index) => {
            const cleanedDate = new Date(book.released).toDateString();
            const authors = book.authors.join(', ');

            return (
              <div className="component" key={index}>
                <h3>Book {index + 1}</h3>
                <h2 style={{ color: "Blue" }}>{book.name}</h2>

                <div className="details">
                  <p> Author: <strong><em>{authors}</em></strong> </p>
                  <p> No. of Pages: <strong><em>{book.numberOfPages}</em> </strong> pages</p>
                  <p> Country:  <strong><em> {book.country}</em> </strong></p>
                  <p> Release Date: <strong><em>  {cleanedDate}</em> </strong></p>
                </div>
              </div>
            );
          })}
      </div>

    </div>
  );
}

